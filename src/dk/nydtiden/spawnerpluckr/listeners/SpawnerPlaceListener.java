package dk.nydtiden.spawnerpluckr.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import dk.nydtiden.spawnerpluckr.SpawnerPluckr;

public class SpawnerPlaceListener implements Listener 
{
	// Class config variable
    private FileConfiguration config;

    private static SpawnerPlaceListener spl;
    
    // Constructor
    protected SpawnerPlaceListener(SpawnerPluckr plugin, FileConfiguration config) 
    {
        this.config = config;
    }
    
    public static SpawnerPlaceListener getInstance(SpawnerPluckr plugin, FileConfiguration config)
	{
		if (spl == null)
		{
			spl = new SpawnerPlaceListener(plugin, config);
		}
		return spl;
    }
    
    @EventHandler
    public void onSpawnerPlace(BlockPlaceEvent e) 
    {
    	Block block = e.getBlock();
    	Material material = block.getType();
    	
    	
        if (material == Material.SPAWNER)
        {
        	Player p = e.getPlayer();
        	boolean hasPermission = p.hasPermission("spawnerpluckr.silkplace");
        	boolean canPlace = config.getBoolean("spawner_place");
        	
        	if(hasPermission && canPlace) 
        	{
        		CreatureSpawner spawner = (CreatureSpawner) block.getState();
        		
        		ItemStack placed = e.getItemInHand();
				ItemMeta meta = placed.getItemMeta();
				String dispName = meta.getDisplayName();

				String entityName = dispName.substring(12).toUpperCase().replace(' ', '_');
				String altEntName = dispName.substring(2, dispName.length()-8).toUpperCase().replace(' ', '_');

				EntityType entity = EntityType.valueOf("SHEEP");
				
				try
				{
					entity = EntityType.valueOf(entityName);
				}
				catch (Exception ex1) {
					try 
					{
						entity = EntityType.valueOf(altEntName);
					}
					catch (Exception ex2)
					{
						ex2.printStackTrace();
					}
				}

				spawner.setSpawnedType(entity);
				spawner.update();
        	}
        	else
        	{
        		e.setCancelled(true);
        		p.sendMessage(ChatColor.DARK_RED + "You are not allowed to place Spawners");
        	}
	        
        }
    }
}
