package dk.nydtiden.spawnerpluckr.listeners;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import dk.nydtiden.spawnerpluckr.SpawnerPluckr;

public class SpawnerBreakListener implements Listener 
{
    // Class config variable
    private FileConfiguration config;

    private static SpawnerBreakListener sbl;
    
    // Constructor
    protected SpawnerBreakListener(SpawnerPluckr plugin, FileConfiguration config) 
    {
        this.config = config;
    }
    
    public static SpawnerBreakListener getInstance(SpawnerPluckr plugin, FileConfiguration config)
	{
		if (sbl == null)
		{
			sbl = new SpawnerBreakListener(plugin, config);
		}
		return sbl;
	
    }

    @EventHandler (priority = EventPriority.MONITOR)
    public void onSpawnerMine(BlockBreakEvent e) 
    {
    	Player player = e.getPlayer();
        Block block = e.getBlock();
        Material material = block.getType();
        
        
        if (material == Material.SPAWNER && !e.isCancelled() && player.getGameMode().equals(GameMode.SURVIVAL)) {
        	boolean require_pick = config.getBoolean("pickaxe");
            boolean require_silk = config.getBoolean("silk_drop");
            
            // Is player allowed to obtain spawners?
            if(player.hasPermission("spawnerpluckr.silkdrop")) 
            {
            	boolean player_silk = player.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH);
            	boolean player_pick = player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("pickaxe");
            	
            	if(player_silk || !require_silk)
            	{
            		if(player_pick || !require_pick)
            		{
            			// Cancel exp drop, or they will be used for xp farming (Place and re-mine)
                        e.setExpToDrop(0);
                        
                        CreatureSpawner spawner = (CreatureSpawner) block.getState();
                        String creature = spawner.getSpawnedType().toString().toLowerCase();
                        
                        
                        ItemStack item = new ItemStack(material);
                        ItemMeta meta = item.getItemMeta();
                        
                        BlockStateMeta bsm = (BlockStateMeta) meta;
                        bsm.setBlockState(spawner);
                        
                        bsm.setDisplayName(ChatColor.DARK_AQUA + "Spawner - " + creature);
                        item.setItemMeta(bsm);
                        
                        block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
            		}
            	}
            	if(player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("boat"))
            	{
            		player.sendMessage(ChatColor.GREEN + "You have too much time, m8");
            	}
            } 
            else 
            {
                return;
            }
        }
    }
}