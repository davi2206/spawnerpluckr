package dk.nydtiden.spawnerpluckr;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import dk.nydtiden.spawnerpluckr.listeners.SpawnerBreakListener;
import dk.nydtiden.spawnerpluckr.listeners.SpawnerPlaceListener;

public class SpawnerPluckr extends JavaPlugin// implements Listener
{
	
	private ConsoleCommandSender clog;
	FileConfiguration config;
	
	@Override
	public void onEnable()
	{
		clog = this.getServer().getConsoleSender();
		// Create or Load Config
        saveDefaultConfig();
        config = getConfig();
        
        // Register Listeners
        getServer().getPluginManager().registerEvents(SpawnerBreakListener.getInstance(this, config), this);
        getServer().getPluginManager().registerEvents(SpawnerPlaceListener.getInstance(this, config), this);

        clog.sendMessage("Spawner Pluckr Enabeled!");
    }

    @Override
    public void onDisable() 
    {
    	HandlerList.unregisterAll();
    	clog.sendMessage("Spawner Pluckr Disabled \n");
    }
}
